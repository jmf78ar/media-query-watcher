# Media Query Watcher jQuery Plugin

module that allows to handle events whenever a media query is applied

## Getting Started

 - Download the project
 - Install node.js
 - Install Grunt and Bower:
   `npm install -g grunt-cli bower`
 - `npm install`
 - `bower install`
 
After doing that, all the dependencies will be downloaded.
Then, use grunt to build and test the component and also run the demo.

### Build and test

Run `grunt`

It will build the development and production (minified) version of the component.

### Develop

Run `grunt server`

It will create a server and watch for file changes as you develop. 

### Test

Run `grunt test`

It will only run the tests.
 
## Examples

Run `grunt demo`


## Usage

...

## Dependencies

The component requires jQuery 1.9.


## Documentation
_(Coming soon)_


